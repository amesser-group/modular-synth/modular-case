###########################
Eurorack Modular Synth Case
###########################

Overview
========

A long-cherished plan of mine is to build and play around with
a synthesizer. So I finally started this project now by creating
a case from leftovers from other projects.

.. image:: doc/images/case.jpg

Because of its widespread use and the many available ready made 
modules I decided to follow the `Döpfer A-100`_ modular system
also known as Eurorack.

.. _Döpfer A-100: https://doepfer.de/home_d.htm

Status
======


A wooden case was built and +-12V Power Supply was designed 
and tested. After a while it turned out that the powersupply 
suffered from `oscillations in Sziklai`_ power stages and needed
some improvements. I have updated the schematics to rev2. 
The pcb files however are outdated and need updating as well.

.. _oscillations in Sziklai: https://electronics.stackexchange.com/questions/474027/sziklai-pair-feedback-loop

The powerdistirbution boards are also ready to use and tested.
Each powesupply board can be optionally equipped with a 5V Buck
regulator.


Licensing
=========

All files of the project are licensed under the conditions of 
CC BY-SA license.


Copyright
=========

For all files in this project folder copyright is
(c) 2022 Andreas Messer <andi@bastelmap.de>.

Links
=====

.. target-notes::




